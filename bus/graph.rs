use crate::component;
use crate::connector;

/// A specification for a graph, including all the routing primitives.
#[derive(Clone)]
pub struct BusSpec {
    /// The name of the bus type.
    name: String,
    
    /// Basic connector for the bus.
    connector: connector::ConnectorSpec
    
    /// The general shape of the bus lane, should be able to be repeated.
    lane_wire_step: component::ComponentSpec,

    /// Same as above, but for when we need repeaters.
    lane_repeater_step: component::ComponentSpec,

    /// Component to turn the bus left.
    turn_left: component::ComponentSpec,

    /// Component to turn the bus right.
    turn_right: component::ComponentSpec,

    /// Component to read data from the bus, going left.
    read_left: component::ComponentSpec,

    /// Component to read data from the bus, going right.
    read_right: component::ComponentSpec,

    /// Component to write data to the bus from the left.
    write_left: component::ComponentSpec,

    /// Component to write data to the bus from the right.
    write_right: component::ComponentSpec,
}

#[derive(Clone)]
pub struct ComponentInst {

    /// The name of this particular instance of a component.  Like "register_a".
    inst_name: String,

    /// Which component spec to use.
    spec: component::ComponentSpec,
}

#[derive(Clone, Debug)]
pub enum BusSide {
    Left,
    Right
}

#[derive(Clone, Debug)]
pub enum AttachmentMode {
    Read,
    Write,
}

#[derive(Clone, Debug)]
pub struct BusAttachment {
    /// The name of the component instance.
    inst_name: String,

    /// Which connector of the component to attach to the bus.
    bus_connection_name: String,

    /// Which side of the bus to connect to.
    side: BusSide,

    /// Specifies whether to use a read or write branch.
    connection_mode: AttachmentMode
}

/// One straight branch of the bus.
#[derive(Clone)]
pub struct BusBranch {
    /// The attachments onto this bus branch, in order.
    attachments: Vec<BusAttachment>
}
