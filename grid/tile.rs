use crate::space::*;
use crate::*;

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Tile {
    /// Air, like, the stuff you breathe.
    Air,

    /// A solid block that can conduct a signal, like stone.
    Solid,

    /// A block that will block connections but not connect a signal.
    Transparent,

    /// Redstone wire.
    Wire,

    /// A repeater in a particular direction at a particular delay setting,
    /// should be 1, 2, 3, or 4.
    Repeater(Cardinal, u8),

    /// A redstone torch with some attachment, should be on a solid block.
    Torch(AttachFace),
}

impl Tile {
    pub fn rotate_ccw(&self) -> Tile {
        use self::Tile::*;
        match *self {
            Air => Air,
            Solid => Solid,
            Transparent => Transparent,
            Wire => Wire,
            Repeater(d, s) => Repeater(d.rotate_ccw(), s),
            Torch(f) => Torch(f.rotate_ccw()),
        }
    }

    pub fn rotate_cw(&self) -> Tile {
        use self::Tile::*;
        match *self {
            Air => Air,
            Solid => Solid,
            Transparent => Transparent,
            Wire => Wire,
            Repeater(d, s) => Repeater(d.rotate_cw(), s),
            Torch(f) => Torch(f.rotate_cw()),
        }
    }

    pub fn rotate_n(&self, n: u8) -> Tile {
        match n % 4 {
            0 => *self,
            1 => self.rotate_ccw(),
            2 => self.rotate_ccw().rotate_ccw(),
            3 => self.rotate_cw(),
            _ => unreachable!(),
        }
    }
}

impl Default for Tile {
    fn default() -> Self {
        Tile::Air
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Cardinal {
    East,
    North,
    West,
    South,
}

impl Cardinal {
    pub fn ordinal(&self) -> u8 {
        use self::Cardinal::*;
        match *self {
            East => 0,
            North => 1,
            West => 2,
            South => 3,
        }
    }

    pub fn from_ordinal(o: u8) -> Cardinal {
        use self::Cardinal::*;
        match o % 4 {
            0 => East,
            1 => North,
            2 => West,
            3 => South,
            _ => unreachable!(),
        }
    }

    pub fn rotate_ccw(&self) -> Cardinal {
        use self::Cardinal::*;
        match *self {
            East => North,
            North => West,
            West => South,
            South => East,
        }
    }

    pub fn rotate_cw(&self) -> Cardinal {
        use self::Cardinal::*;
        match *self {
            East => South,
            North => East,
            West => North,
            South => West,
        }
    }

    fn rotate_n(&self, n: u8) -> Cardinal {
        match n {
            0 => *self,
            1 => self.rotate_ccw(),
            2 => self.rotate_ccw().rotate_ccw(),
            3 => self.rotate_cw(),
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum AttachFace {
    Upright,
    Face(Cardinal),
}

impl AttachFace {
    fn rotate_ccw(&self) -> AttachFace {
        use self::AttachFace::*;
        match *self {
            Upright => Upright,
            Face(d) => Face(d.rotate_ccw()),
        }
    }

    fn rotate_cw(&self) -> AttachFace {
        use self::AttachFace::*;
        match *self {
            Upright => Upright,
            Face(d) => Face(d.rotate_cw()),
        }
    }

    fn rotate_n(&self, n: u8) -> AttachFace {
        if n > 4 {
            self.rotate_n(n.mod_euc(4))
        } else {
            match n {
                0 => *self,
                1 => self.rotate_ccw(),
                2 => self.rotate_ccw().rotate_ccw(),
                3 => self.rotate_cw(),
                _ => unreachable!(),
            }
        }
    }
}

// impl of custom marker trait
impl SpaceCell for Tile {}
